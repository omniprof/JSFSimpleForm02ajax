-- Best practice MySQL as of 5.7.6
--
-- This script needs to run only once

DROP DATABASE IF EXISTS JSFEXERCISE;
CREATE DATABASE JSFEXERCISE;

USE JSFEXERCISE;

DROP USER IF EXISTS user@localhost;
CREATE USER user@'localhost' IDENTIFIED WITH mysql_native_password BY 'pencil' REQUIRE NONE;
GRANT ALL ON JSFEXERCISE.* TO user@'localhost';

FLUSH PRIVILEGES;
