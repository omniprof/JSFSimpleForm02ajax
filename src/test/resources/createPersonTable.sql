-- Best practice MySQL as of 5.7.6
--
USE JSFEXERCISE;

DROP TABLE IF EXISTS PERSON;
CREATE TABLE PERSON (
  ID int NOT NULL auto_increment,
  NAME varchar(45) NOT NULL default '',
  AGE int NOT NULL default 0,
  WEIGHT int NOT NULL default 0,
  HEIGHT int NOT NULL default 0,
  HAIRCOLOUR varchar(25) NOT NULL default '',
  PRIMARY KEY (ID)
) ENGINE=InnoDB;

